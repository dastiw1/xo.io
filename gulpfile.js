/**
 * Created by dastan on 19/04/2016.
 */
var gulp = require('gulp');

var bower = require('gulp-bower');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var concatCss = require('gulp-concat-css');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var notify = require("gulp-notify");


var path = {
    bower: 'bower_components/',
    js: 'js/',
    css: 'css/'
};
var config = {
    jsFiles: [
        path.bower + 'jquery-legacy/dist/jquery.min.js',
        path.js + 'scripts.js'
    ],
    cssFiles: [
        path.css + 'style.css',

    ],
    fontFiles: [
        'bower_components/bootstrap-sass/assets/fonts/**'
    ],
    imgFiles: [],
    sassPath: [
        'scss/*.scss',
        'scss/bootstrap/*.scss',
        'scss/bootstrap/mixins/*.scss',
        './scss/*.scss',

    ],
    sassWatchPath: [
        'scss/*.scss',
        'scss/bootstrap/*.scss',
        'scss/bootstrap/mixins/*.scss',
        './scss/*.scss',

    ]
};


gulp.task('bower', function () {
    return bower();
});


gulp.task('font', function () {
    return gulp.src(config.fontFiles)
        .pipe(gulp.dest('./fonts'));
});

gulp.task('images', function () {
    return gulp.src(config.imgFiles)
        .pipe(gulp.dest('./dist'));
});


gulp.task('css', function () {
    return gulp.src(config.cssFiles)
        .pipe(concatCss("styles.css"))
        .pipe(gulp.dest('./dist'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('./dist'));
});


gulp.task('script', function () {
    return gulp.src(config.jsFiles)
        .pipe(concat("scripts.js"))
        .pipe(uglify())
        .pipe(rename('scripts.min.js'))
        .pipe(gulp.dest("./js"));
});
gulp.task('sass', function () {
    return gulp.src(config.sassPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    gulp.watch(config.sassWatchPath, ['sass']);
});

gulp.task('watch', function () {
    gulp.watch('./scss/*.scss', ['sass', 'css']);
    gulp.watch(config.cssFiles ['css']);
    gulp.watch(config.jsFiles, ['script']);
    gulp.watch(config.fontFiles, ['font']);
    gulp.watch(config.imgFiles, ['images']);
});